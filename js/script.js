   if($(".popup_open").length){
      include("js/jquery.arcticmodal.js"); 
    }

   function include(url){
    document.write('<script src="'+ url + '"></script>');
  }


$(function(){
// IPad/IPhone
  var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
  ua = navigator.userAgent,

  gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

  scaleFix = function () {
    if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
      viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
      document.addEventListener("gesturestart", gestureStart, false);
    }
  };
  
  scaleFix();
  // Menu Android
  
});

var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
  if(!result){
    userScale=",user-scalable=0"
  }
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')
// select
  $.fn.extend({

    /**** Emulates select form element
    ** @return jQuery**/
    customSelect : function(){
      var template = "<div class='active_option open_select'></div><ul class='options_list dropdown'></ul>";

      return this.each(function(){

      var $this = $(this);
      $this.prepend(template);

      var active = $this.children('.active_option'),
       list = $this.children('.options_list'),
       select = $this.children('select').hide(),
       options = select.children('option'),
       iClass = active.attr('class'),
       position = $this.offset().top;

       console.log(position);


      active.text( 
       select.children('option[selected]').val() ? 
        select.children('option[selected]').val() : 
        options.eq(0).text()
      );

      active.on('click', function(){
        $this.toggleClass('active');
      });

      options.each(function(){

       var optionOuter = $('<li></li>'),
       span = $('<span></span>'),
       optionInner = $('<a></a>',{
         text : $(this).text(),
         href : '#',
         'data-value' : $(this).val(),
         class: $(this).data('flag-class')
        }),
        tpl = optionOuter.append(optionInner.append(span));


       list.append(tpl);
       
      });

      var listHeight = list.outerHeight();

      console.log($(document).height(), listHeight);

      if($(document).height() - position < listHeight){
        list.addClass('top_position');
      }

      list.on("click", "a", function(event){

       event.preventDefault();

       var t = $(this).text(),
          v = $(this).attr('data-value');
       active.text(t);

       iClass.replace(iClass,"active_option open_select");

       active.attr('class', iClass +" "+ $(this).attr("class"));

       select.val(v);

       $(this).closest('.dropdown').removeClass("active");

      });

      $(document).click(function(event) {
        if ($(event.target).closest(".open_select").length) return;
        $(".custom_select").removeClass("active");
        event.stopPropagation();
      });

      });

  }});


  

$(document).ready(function(){

    $('.custom_select').customSelect();

//----------

  if($(".fortooltip_aba").length){

    $(".fortooltip_aba").on("click", function(){
        $(this).next().toggleClass("active");

    });
  }

//-------popup

  $('.popup_open').on('click',function(){
    var modal = $(this).data("modal");
    $(modal).arcticmodal();

   });

})






